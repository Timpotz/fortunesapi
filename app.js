const fs = require ('fs');
const express = require('express');
const app = express();
const fortunes = require('./data/fortunes.json');
const bodyParser = require('body-parser');

//middleware
app.use(bodyParser.json());

//helper
const writeFortunes = json =>{
    fs.writeFile('./data/fortunes.json', JSON.stringify(json), err =>console.log(err));
};

app.get('/fortunes',(req,res)=> {
    res.json(fortunes);
});

app.get('/fortunes/random',(req,res) =>{
    // console.log ('requesting random fortunes')
    //randomize the index
    // const random_index = Math.floor(Math.random() * fortunes.length)
    const r_fortune = fortunes[Math.floor(Math.random() * fortunes.length)];
    res.json(r_fortune);
    
});

app.get('/fortunes/:id',(req,res)=>{
    // console.log(req.params)
    res.json(fortunes.find(f => f.id == req.params.id))
});

app.post('/fortunes',(req,res)=>{
    const{message,lucky_number,spirit_animal} = req.body;
    const fortune_ids =fortunes.map(f=>f.id);
    const new_fortunes =fortunes.concat({
        id: (fortune_ids.length >0? Math.max(...fortune_ids): 0)+1,
        message,
        lucky_number,
        spirit_animal
    });

    writeFortunes(new_fortunes);

    res.json(new_fortunes);
});

app.put('/fortunes/:id',(req,res)=>{
    const{id} = req.params;
    // const{message,lucky_number,spirit_animal} = req.body;
    //simplified below
    const old_fortune = fortunes.find(f =>f.id == id);
    
    // if(message) old_fortune.message = message;
    // if(lucky_number) old_fortune.lucky_number = lucky_number;
    // if(spirit_animal) old_fortune.spirit_animal = spirit_animal;
    // simplified below
    ['message','lucky_number','spirit_animal'].forEach(key=>{
        if(req.body[key]) old_fortune[key] =req.body[key];
        // if(req.body[key==0]) old_fortune[key] = null
    });

    writeFortunes(fortunes);

    res.json(fortunes);
});

app.delete('/fortunes/:id', (req,res)=>{
    const {id} = req.params;
    const new_fortunes = fortunes.filter(f=>f.id !=id);

    writeFortunes(new_fortunes);
    res.json(new_fortunes);

})
// const fortune_ids =fortunes.map(f=>f.id)
// console.log(fortune_ids)
// const random_index = Math.floor(Math.random() * fortunes.length)
// console.log(fortunes [random_index])
// //get the random index
// console.log (Math.floor(Math.random() * fortunes.length))
module.exports = app;